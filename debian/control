Source: django-sitetree
Section: python
Priority: optional
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Michael Fladischer <fladi@debian.org>,
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all,
 python3-babel,
 python3-django,
 python3-pytest,
 python3-pytest-djangoapp,
 python3-setuptools,
 python3-sphinx,
Standards-Version: 4.6.2
Homepage: https://github.com/idlesign/django-sitetree
Vcs-Git: https://salsa.debian.org/python-team/packages/django-sitetree.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/django-sitetree
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python-django-sitetree-doc
Section: doc
Architecture: all
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Multi-Arch: foreign
Description: site tree, menu and breadcrumbs navigation for Django (Documentation)
 django-sitetree is a reusable application for Django, introducing site tree,
 menu and breadcrumbs navigation elements. Site structure in django-sitetree is
 described through Django admin interface in a so called site trees. Every item
 of such a tree describes a page or a set of pages through the relation of URI
 or URL to human-friendly title.
 .
 This package contains the documentation.

Package: python3-django-sitetree
Architecture: all
Depends:
 python3-django,
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-django-sitetree-doc,
Description: site tree, menu and breadcrumbs navigation for Django (Python3 version)
 django-sitetree is a reusable application for Django, introducing site tree,
 menu and breadcrumbs navigation elements. Site structure in django-sitetree is
 described through Django admin interface in a so called site trees. Every item
 of such a tree describes a page or a set of pages through the relation of URI
 or URL to human-friendly title.
 .
 This package contains the Python 3 version of the library.
